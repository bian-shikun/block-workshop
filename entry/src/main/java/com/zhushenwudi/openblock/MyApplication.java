package com.zhushenwudi.openblock;

import com.orhanobut.logger.HarmonyLogAdapter;
import com.orhanobut.logger.Logger;
import net.grandcentrix.tray.AppPreferences;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {

    private static MyApplication instance;
    private static AppPreferences appPreferences;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
        instance = this;
        Logger.addLogAdapter(new HarmonyLogAdapter());
        appPreferences = new AppPreferences(getContext());
    }

    public AppPreferences getSp() {
        return appPreferences;
    }
}
