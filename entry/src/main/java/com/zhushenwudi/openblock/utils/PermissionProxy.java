package com.zhushenwudi.openblock.utils;

import com.permissions.dispatcher.annotation.*;
import com.zhushenwudi.openblock.MyApplication;

public class PermissionProxy {
    @NeedsPermission("")
    public void toOpenPermission() {
        Toast.myToast(MyApplication.getInstance().getContext(), "权限已申请成功");
    }

    @OnPermissionDenied("")
    public void toResetPermission() {
        Toast.myToast(MyApplication.getInstance().getContext(), "权限被拒绝了");
    }

    @OnShowRationale("")
    public void showDialog(PermissionRequest request) {
        Toast.myToast(MyApplication.getInstance().getContext(), "我们需要你的授权");
        // 确定申请
        request.proceed();
        // 取消
//        request.cancel();
    }

    @OnNeverAskAgain("")
    public void toExplainPermission() {
        Toast.myToast(MyApplication.getInstance().getContext(), "解释这个权限为什么被拒绝");
    }
}
