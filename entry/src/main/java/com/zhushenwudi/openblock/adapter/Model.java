package com.zhushenwudi.openblock.adapter;

public interface Model {
    /**
     * 当前Model配置布局文件
     *
     * @param position 当前Model在数组中的位置
     * @return 返回布局文件
     */
    int getResource(int position);

    /**
     * 返回模型对应的ViewHolder
     *
     * @param position 当前Model在数组中的位置
     * @return 返回当前布局文件对应的ViewHolder类
     */
    Class getHolderClass(int position);
}