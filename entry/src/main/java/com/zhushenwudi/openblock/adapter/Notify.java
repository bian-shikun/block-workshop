package com.zhushenwudi.openblock.adapter;

public interface Notify {

    void notifyDataChanged();

    void notifyDataInvalidated();

    void notifyDataSetItemChanged(int position);

    void notifyDataSetItemInserted(int position);

    void notifyDataSetItemRemoved(int position);

    void notifyDataSetItemRangeChanged(int startPos, int countItems);

    void notifyDataSetItemRangeInserted(int startPos, int countItems);

    void notifyDataSetItemRangeRemoved(int startPos, int countItems);
}