package com.zhushenwudi.openblock.holder;

import com.zhushenwudi.openblock.ResourceTable;
import com.zhushenwudi.openblock.adapter.EventTransmissionListener;
import com.zhushenwudi.openblock.adapter.SuperProvider;
import com.zhushenwudi.openblock.adapter.ViewHolder;
import com.zhushenwudi.openblock.model.Device;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class RemoteHolder extends ViewHolder<Device> {
    private final Text tvDeviceName;

    public RemoteHolder(EventTransmissionListener listener, Component component, SuperProvider provider) {
        super(listener, component, provider);
        tvDeviceName = (Text) findComponentById(ResourceTable.Id_tvDeviceName);
        Button btnPost = (Button) findComponentById(ResourceTable.Id_btnPost);
        btnPost.setClickedListener(v -> listener.onEventTransmission(component, getModel().getDeviceId(), 1, null));
    }

    @Override
    public void onDataBound() {
        tvDeviceName.setText(getModel().getDeviceName());
    }
}