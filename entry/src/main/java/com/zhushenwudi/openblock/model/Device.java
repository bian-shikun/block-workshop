package com.zhushenwudi.openblock.model;

import com.zhushenwudi.openblock.ResourceTable;
import com.zhushenwudi.openblock.adapter.Model;
import com.zhushenwudi.openblock.holder.RemoteHolder;
import ohos.distributedschedule.interwork.DeviceInfo;

public class Device extends DeviceInfo implements Model {

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_item_remote;
    }

    @Override
    public Class getHolderClass(int position) {
        return RemoteHolder.class;
    }
}
